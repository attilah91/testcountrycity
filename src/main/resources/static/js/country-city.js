$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket - fixed
	*/
	var countryList;
	$.get('/rest/countries', function( countryList ){
		countryList = countryList;
		$.each(countryList, function(i, country){
			$('#city-country').append($('<option>', {
				value: country.iso,
			    text: country.name
			}))
		});
		
	});

	$('body').on('click', '#add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut... - fixed?
		*/
		/*var c; 
		$.each(countryList, function(i, country){
			if (country.iso == $('#city-country').val()) c = country;
		});*/
		var cityData = {
			/*
				TODO: összeszedni az űrlap adatait
			*/
			name: $('#city-name').val(),
			population: $('#city-population').val(),
			'country.iso': $('#city-country').val()
		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
				$('#add-city-modal').modal('hide');
			}
		});
	});

	/*
		TODO: ország törlés gombok működjenek - fixed
	*/
	$('tbody').on('click', '.delete-country', function (){
		var countryISO = $(this).closest('tr').children().first().text();
		$.ajax({
			url: '/rest/countries/'+countryISO,
			method : 'DELETE',			
		});
		$(this).closest('tr').remove();
	});

	/*
		TODO: város törlés gombok működjenek
	*/
	$('tbody').on('click', '.delete-city', function (){
		var cityISO = $(this).closest('tr').children().first().text();
		$.ajax({
			url: '/rest/cities/'+cityISO,
			method : 'DELETE',			
		});
		$(this).closest('tr').remove();
	});

/*	$('tbody').on('click', '.modify-city', function (){
		var cityData = $(this).closest('tr').children();
	
		$('#city-name').val(cityData.eq(0).text());
		$('#city-population').val(cityData.eq(1).text());
		$('#city-country').val(cityData.eq(2).text());
		$('#add-city-modal').modal('toggle');
	});*/

});
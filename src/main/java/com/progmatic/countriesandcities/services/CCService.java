/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.services;

import com.progmatic.countriesandcities.daos.CityAutoDao;
import com.progmatic.countriesandcities.daos.CountryAutoDao;
import com.progmatic.countriesandcities.dtos.CityDto;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.entities.City;
import com.progmatic.countriesandcities.entities.Country;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author peti
 */
@Service
public class CCService {
    
    private static final Logger LOG = LoggerFactory.getLogger(CCService.class);

    @Autowired
    CountryAutoDao countryAutoDao;
    
    @Autowired
    CityAutoDao cityAutoDao;

    @Autowired
    private DozerBeanMapper beanMapper;

    @PersistenceContext
    EntityManager em;

    public List<CountryDto> listAllCountries() {
        //TODO get them from the db - fixed
        List<Country> allCountries = em.createQuery(
                "SELECT country FROM Country country").getResultList();
        //List<Country> allCountries = new ArrayList<>();
        List<CountryDto> allCountryDtos = new ArrayList<>();
        allCountries.forEach((country) -> {
            allCountryDtos.add(beanMapper.map(country, CountryDto.class, "countryMapWithoutCities"));
        });
        return allCountryDtos;
    }
    
    public List<CityDto> listAllCities() {
        //TODO get them from the db - fixed
        //List<City> allCities = new ArrayList<>();;
        List<City> allCities = em.createQuery(
                "SELECT city FROM City city").getResultList();
        List<CityDto> allCountryDtos = new ArrayList<>();
        allCities.forEach((country) -> {
            allCountryDtos.add(beanMapper.map(country, CityDto.class));
        });
        return allCountryDtos;
    }

    //TODO not finsihed - fixed
    public CountryDto detailedCountryData(String id) {
        //TODO load c from the database
        //make somehow sure that all it's cities are also loaded
        Country c = (Country)em.createQuery(
                "SELECT c FROM Country c WHERE c.iso like :ciso").setParameter("ciso", id).getSingleResult();
        //Country c = null;
        if (c == null) {
            return null;
        }
        
        CountryDto cdto = beanMapper.map(c, CountryDto.class);
        return cdto;
    }

    //TODO: not implemented 
    //delete the country and make sure somehow that all of it's cities are also deleted.
    @Transactional
    public void deleteCountry(String id) {
        Country c = em.find(Country.class, id);
        LOG.info("Country deleted: "+ c.getName());
        em.remove(c);
    }

    @Transactional
    public void deleteCity(String id) {
        City c = (City)em.createQuery(
                "SELECT city FROM City city where city.name like :name").setParameter("name",id).getSingleResult();
//        City c = em.find(City.class, id);
        Country country = c.getCountry();
        City capital = country.getCapital();
        if(capital != null && capital.getId().equals(c.getId())){
            LOG.info("Capital of {} is deleted.", country.getName());
            country.setCapital(null);
        }
        em.remove(c);
    }
    
    @Transactional
    public CityDto createCity(CityDto cdto) {
        cdto.setId(UUID.randomUUID().toString());
        City city = beanMapper.map(cdto, City.class);  
        //TODO create the city in the db. 
        //Make sure that it gets an id (maybe use UUID as in BaseEntity) and that the returned cdto also gets it
        em.persist(city);
        return cdto;
    }
    
    @Transactional
    public void modifyCity(CityDto cdto){
        City c = (City)em.createQuery(
                "SELECT city FROM City city where city.name like :name").setParameter("name",cdto.getId()).getSingleResult();
        
    }
}

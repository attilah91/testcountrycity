/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.controllers;

import com.progmatic.countriesandcities.dtos.CityDto;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.services.CCService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author peti
 */
@RestController
@RequestMapping("rest")
public class CCRestController {
    @Autowired
    CCService cCService;
    
    @RequestMapping("countries")
    public List<CountryDto> allCountries(){
        return cCService.listAllCountries();
    }
    
    @RequestMapping(path = "countries/{iso}", method = RequestMethod.GET)
    public CountryDto allCountries(@PathVariable("iso") String countryIso){
        return cCService.detailedCountryData(countryIso);
    }
    
    @RequestMapping(path = "countries/{iso}", method = RequestMethod.DELETE)
    public String deleteCountry(@PathVariable("iso") String countryIso){
        cCService.deleteCountry(countryIso);
        return "ok";
    }
    
    @RequestMapping(path = "cities/{id}", method = RequestMethod.DELETE)
    public String deletecity(@PathVariable("id") String id){
        cCService.deleteCity(id);
        return "ok";
    }
    
    @RequestMapping(path = "cities", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public CityDto addCity(CityDto cdto){
        return cCService.createCity(cdto);
    }
    
    @RequestMapping(path = "cities", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String modifyCity(CityDto cdto){
        cCService.modifyCity(cdto);
        return "ok";
    }
}
